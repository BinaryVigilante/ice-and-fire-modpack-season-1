# Ice and Fire Modpack - Season 1

My Ice and Fire modpack for my Ice and fire let's play series season 1.


**Installation:**

  * Download the **.zip** file.
  * Navigate to your *.minecraft* folder.
  * Make sure that you don't have a "mods" folder nor a "configs" folder. If you have them and would like to save your previous configs and mods, rename your folders to something like "mods-old" and "configs-old". Otherwise delete them.
  * Extract the **.zip** file inside your *.minecraft* folder.
  * Download and install the forge for Minecraft **1.12.2**: https://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html
  * Run minecraft and run with the forge profile you just installed.
  * Enjoy :)

**FAQ:**

*Q: Will you upload the modpack for curseforge/twitch client?*

*A: No, not at this point in time. I wish to not support their modpack platform system as they force you to upload your pack through their client.*


**Issues:**

If you have any issues please make a new issue in the issues tab. _Issues given in video comments and in Discord may **not** be responded to._